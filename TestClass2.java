public class TestClass2 {

    public String brand;
    public String model;
    public Integer year;
    public Boolean used;

    public TestClass2(String brand, String model, Integer year, Boolean used){
        this.brand = brand;
        this.model = model;
        this.year = year;
        this.used = used;
    }

    public TestClass2(String brand, String model, Integer year){
        this.brand = brand;
        this.model = model;
        this.year = year;
    }

    public TestClass2(String brand, String model, Boolean used){
        this.brand = brand;
        this.model = model;
        this.used = used;
    }

    public TestClass2(String brand, String model){
        this.brand = brand;
        this.model = model;
    }

    public void printData(){
        System.out.println("Марка " + brand);
        System.out.println("Модель " + model);

        if (year == null){
            System.out.println("Год выпуска машины неизвестен");
        }
        else {
            System.out.println("Год выпуска " + year);
        }

        if (used == null){
            System.out.println("Неизвестно новая ли машина или б/у");
        }
        else {
            System.out.println("Б/у " + used);
        }
    }

}
